class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable

  before_create :set_api_token
  has_many :posts




  private
  	def set_api_token
  		self.api_token = SecureRandom.base64
  	end

end
