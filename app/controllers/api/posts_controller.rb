class Api::PostsController < ApiController
  skip_before_filter :verify_authenticity_token
	before_filter :find_post, only: [:show, :update, :destroy]



  def index
    render json: Post.where('user_id = ?', @user.id)
  end

  def show
    render json: @post
  end

  def create
    if @post.present?
      render nothing: true, status: :conflict
    else
      @post = Post.new
      @post.assign_attributes(@json['post'])
      @post.user_id = @user.id
      if @post.save
        render json: @post
      else
         render json: {:status => "Failed To Create "}
      end
    end
  end

  def update
    @post.assign_attributes(@json['post'])
    @post.user_id = @user.id
    if @post.save
        render json: @post
    else
        render json: {:status => "Failed To Update "}
    end
  end

  def destroy
    if @post.destroy
      render json: {:status => "Deleted"}
    else
      render json: {:status => "Failed To Delete"}
    end
  end

 private
   def find_post
     @post = Post.where("id = ? and user_id = ?", params[:id], @user.id).last
     render json: {:status => "Record Not Found "} unless @post.present? && @post.user_id == @user.id
   end

end