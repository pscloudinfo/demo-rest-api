 class ApiController < ApplicationController
    before_filter :parse_request, :authenticate_user_from_token! 
    private
       def authenticate_user_from_token!
         if !@api_token
           render json: {:status => "Unauthorized User "}
         else
           @user = nil
           User.find_each do |u|
             if Devise.secure_compare(u.api_token, @api_token)
               @user = u
             end
           end
           if @user.nil?
              render json: {:status => "Unauthorized User "}
           end
         end
       end

       def parse_request
         @json = JSON.parse(request.body.read) if request.body.read.present?
         @api_token = request.headers["Authorization"]
       end
    end