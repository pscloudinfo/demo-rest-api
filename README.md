# README

This README would normally document whatever steps are necessary to get the
application up and running.

Things you may want to cover:

* Ruby version Version => 2.3.1
* Rails Version =>  5.0.2


Run rake db:seed  To Create Users
Refer seeds.rb

Please Use Headers To Access Api's
	Content-Type => application/json
	Accet => application/json
	Authorization => user api_token will genereate when ever user is created

	 
Request => GET api/posts/1.json? #Show
Response => {
	"id": 1,
	"title": "abc11221",
	"url": "abc.com",
	"user_id": 2,
	"created_at": "2018-04-12T20:17:25.706Z",
	"updated_at": "2018-04-12T20:33:44.395Z"
}	

Request => GET api/posts.json? #Index
Response => [
{
	"id": 1,
	"title": "abc11221",
	"url": "abc.com",
	"user_id": 2,
	"created_at": "2018-04-12T20:17:25.706Z",
	"updated_at": "2018-04-12T20:33:44.395Z"
},
{
	"id": 3,
	"title": "abc11221222",
	"url": "abc22222.com",
	"user_id": 2,
	"created_at": "2018-04-15T19:54:41.727Z",
	"updated_at": "2018-04-15T19:54:41.727Z"
}
]

Request => POST api/posts.json? #Creation
Body Params => { "post" : {"title" : "abc11221222","url" :  "abc22222.com"}}
Response => [
{
	"id": 3,
	"title": "abc11221222",
	"url": "abc22222.com",
	"user_id": 2,
	"created_at": "2018-04-15T19:54:41.727Z",
	"updated_at": "2018-04-15T19:54:41.727Z"
}
]	


Request => PUT api/posts/3.json? #Updation
Body Params => { "post" : {"title" : "abc11","url" :  "abc22.com"}}
Response => [
{
"id": 3,
"title": "abc11",
"url": "abc22.com.com",
"user_id": 2,
"created_at": "2018-04-15T19:54:41.727Z",
"updated_at": "2018-04-15T19:54:41.727Z"
}
]	


